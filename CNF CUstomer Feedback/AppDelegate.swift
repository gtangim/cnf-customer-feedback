//
//  AppDelegate.swift
//  CNF CUstomer Feedback
//
//  Created by Anik on 2017-09-23.
//  Copyright © 2017 CNFITCommunity Natural Foods. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    class CustomerFeedback {
        var feedback : String = ""
        var reason : String = ""
        var locationId : String = ""
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        Timer.scheduledTimer(timeInterval: 60.0, target: self, selector: #selector(getRecordsToUpload), userInfo: nil, repeats: true)

        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "CNF_CUstomer_Feedback")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    
    
    //MARK: - Web service call functions
    @objc func getRecordsToUpload() {
        print("Getting list of recrds to upload")
        
        let context = persistentContainer.viewContext
        let feedbackFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "CustomerFeedback")
        let uploaded = false
        feedbackFetch.predicate = NSPredicate(format: "uploaded == %@", uploaded as CVarArg)
        
        var feedbacks: [NSManagedObject] = []

        do {
            feedbacks = try context.fetch(feedbackFetch) as! [NSManagedObject]
            
            if(feedbacks.count==0) {print("no new records to upload")}
            
            for feedback:NSManagedObject in feedbacks{
                submitToServer(feedbackObject:feedback)
            }
            
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    
    func submitToServer(feedbackObject: NSManagedObject){
        
        //populate object
        
        let feedback = CustomerFeedback()
        feedback.feedback = feedbackObject.value(forKey: "feedback") as! String
        feedback.reason = feedbackObject.value(forKey: "reason") as! String
        feedback.locationId = feedbackObject.value(forKey: "storeLocation") as! String
        
        var request = URLRequest(url: URL(string: "http://webapp.cnfltd.com/cnfmobilewebservice/CNFMobileService.asmx/SubmitCustomerFeedback")!)
        
        request.httpMethod = "POST"
        let postString = JSONSerializer.toJson(feedback)

        request.httpBody = postString.data(using: .utf8)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue( String(postString.characters.count), forHTTPHeaderField: "Content-Length")

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(String(describing: error))")
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(String(describing: response))")
                return
            }
            
            let responseString = String(data: data, encoding: .utf8)
           
            if responseString?.range(of:"Success") != nil {
                print("record uploaded successfully")
                
                //change the record flag uploaded to true
                
                let context = self.persistentContainer.viewContext
                let feedbackFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "CustomerFeedback")
                feedbackFetch.predicate = NSPredicate(format: "uploaded == %@ AND dateTime == %@", feedbackObject.value(forKey: "uploaded") as! CVarArg, feedbackObject.value(forKey: "dateTime") as! CVarArg)
                
                var feedbacks: [NSManagedObject] = []
                
                do {
                    feedbacks = try context.fetch(feedbackFetch) as! [NSManagedObject]
                    
                    for feedback:NSManagedObject in feedbacks{
                        feedback.setValue(true, forKeyPath: "uploaded")
                    }
                    
                    try context.save()
                    
                } catch let error as NSError {
                    print("Could not update uploaded flag. \(error), \(error.userInfo)")
                }
            }else{
                print("Failed to submit to Web service, Will try later")
            }
        }
        task.resume()
    }
    
    
    

}

