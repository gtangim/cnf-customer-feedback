//
//  HomeViewController.swift
//  CNF CUstomer Feedback
//
//  Created by Anik on 2017-09-23.
//  Copyright © 2017 CNFITCommunity Natural Foods. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    @IBOutlet weak var homegifViewer: UIImageView!
    var feedback : String = ""
    var location : Dictionary< String , String>? = nil
    @IBOutlet weak var locationLabel: UILabel!
    
    //button outlets
    @IBOutlet weak var veryHappyButton: UIButton!
    @IBOutlet weak var happyButton: UIButton!
    @IBOutlet weak var neutralButton: UIButton!
    @IBOutlet weak var sadButton: UIButton!
    @IBOutlet weak var angryButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        //homegifViewer.loadGif(name: "giphy-4")
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        if(location==nil){ self.locationLabel.text = "Please select Location to Precede"}
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if(location==nil){ self.locationLabel.text = "Please select Location to Precede"}
        else{
            //self.locationLabel.text = (location?["Description"])! + " @ Community Natural Foods Ltd."
        }
        loadButtonImages()
        activeAllButtons()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
        
    //MARK: - button load funciton on viewload
    
    func loadButtonImages(){
        veryHappyButton.setImage(UIImage(named: "superHappy_active.png"), for: UIControlState.normal)
        happyButton.setImage(UIImage(named: "happy_active.png"), for: UIControlState.normal)
        neutralButton.setImage(UIImage(named: "natural_active.png"), for: UIControlState.normal)
        sadButton.setImage(UIImage(named: "sad_active.png"), for: UIControlState.normal)
        angryButton.setImage(UIImage(named: "veryAngry_active.png"), for: UIControlState.normal)
    }
    
    
    // MARK: - button clicked functions
    
    @IBAction func veryHappyClicked(_ sender: Any) {
        veryHappyButton.setImage(UIImage(named: "superHappy_active.png"), for: UIControlState.normal)
        happyButton.setImage(UIImage(named: "happy_inactive.png"), for: UIControlState.normal)
        neutralButton.setImage(UIImage(named: "natural_inactive.png"), for: UIControlState.normal)
        sadButton.setImage(UIImage(named: "sad_inactive.png"), for: UIControlState.normal)
        angryButton.setImage(UIImage(named: "veryAngry_inactive.png"), for: UIControlState.normal)
        feedback = "VERY_SATISFIED"
        animateButton(button: veryHappyButton)
        
    }
    @IBAction func happyClicked(_ sender: Any) {
        veryHappyButton.setImage(UIImage(named: "superHappy_inactive.png"), for: UIControlState.normal)
        happyButton.setImage(UIImage(named: "happy_active.png"), for: UIControlState.normal)
        neutralButton.setImage(UIImage(named: "natural_inactive.png"), for: UIControlState.normal)
        sadButton.setImage(UIImage(named: "sad_inactive.png"), for: UIControlState.normal)
        angryButton.setImage(UIImage(named: "veryAngry_inactive.png"), for: UIControlState.normal)
        feedback = "SATISFIED"
        animateButton(button: happyButton)
        
    }
    @IBAction func neutralClicked(_ sender: Any) {
        veryHappyButton.setImage(UIImage(named: "superHappy_inactive.png"), for: UIControlState.normal)
        happyButton.setImage(UIImage(named: "happy_inactive.png"), for: UIControlState.normal)
        neutralButton.setImage(UIImage(named: "natural_active.png"), for: UIControlState.normal)
        sadButton.setImage(UIImage(named: "sad_inactive.png"), for: UIControlState.normal)
        angryButton.setImage(UIImage(named: "veryAngry_inactive.png"), for: UIControlState.normal)
        feedback = "MODERATE"
        animateButton(button: neutralButton)
        
    }
    @IBAction func sadClicked(_ sender: Any) {
        veryHappyButton.setImage(UIImage(named: "superHappy_inactive.png"), for: UIControlState.normal)
        happyButton.setImage(UIImage(named: "happy_inactive.png"), for: UIControlState.normal)
        neutralButton.setImage(UIImage(named: "natural_inactive.png"), for: UIControlState.normal)
        sadButton.setImage(UIImage(named: "sad_active.png"), for: UIControlState.normal)
        angryButton.setImage(UIImage(named: "veryAngry_inactive.png"), for: UIControlState.normal)
        feedback = "UNSATISFIED"
        animateButton(button: sadButton)
    }
    @IBAction func angryClicked(_ sender: Any) {
        veryHappyButton.setImage(UIImage(named: "superHappy_inactive.png"), for: UIControlState.normal)
        happyButton.setImage(UIImage(named: "happy_inactive.png"), for: UIControlState.normal)
        neutralButton.setImage(UIImage(named: "natural_inactive.png"), for: UIControlState.normal)
        sadButton.setImage(UIImage(named: "sad_inactive.png"), for: UIControlState.normal)
        angryButton.setImage(UIImage(named: "veryAngry_active.png"), for: UIControlState.normal)
        feedback = "VERY_UNSATISFIED"
        animateButton(button: angryButton)
    }
    
    
    //MARK: - button activity funtions
    
    func inactiveAllButtons(){
        veryHappyButton.isEnabled = false
        happyButton.isEnabled = false
        neutralButton.isEnabled = false
        sadButton.isEnabled = false
        angryButton.isEnabled = false
    }
    
    func activeAllButtons(){
        veryHappyButton.isEnabled = true
        happyButton.isEnabled = true
        neutralButton.isEnabled = true
        sadButton.isEnabled = true
        angryButton.isEnabled = true
    }
    
    
    func animateButton(button: UIButton) {
        inactiveAllButtons()
        
        button.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        
        UIView.animate(withDuration: 1.0,
                       delay: 0,
                       usingSpringWithDamping: 0.2,
                       initialSpringVelocity: 6.0,
                       options: .allowUserInteraction,
                       animations: { button.transform = .identity
            },
                       completion:{
                        (value: Bool) in
                        
//                        if(self.feedback == "UNSATISFIED" || self.feedback == "VERY_UNSATISFIED"){
//                            self.performSegue(withIdentifier: "homeToFeedbackSegue", sender: self)
//                        }else{
                            self.performSegue(withIdentifier: "homeToThanksSegue", sender: self)
//                        }
                    
            })
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "homeToThanksSegue"){
            if let thanks = segue.destination as? ThanksYouViewController {
             
                thanks.locationId = (location?["LocationId"])!
                thanks.feedback = feedback
                
            }
        }else if(segue.identifier == "homeToFeedbackSegue"){
            if let feedbackContrl = segue.destination as? FeedbackReasonTableViewController {
                
                feedbackContrl.locationId = (location?["LocationId"])!
                feedbackContrl.feedback = feedback
            }
        }
    }
    
}
