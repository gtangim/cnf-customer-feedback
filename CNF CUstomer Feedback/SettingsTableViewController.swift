//
//  SettingsTableViewController.swift
//  CNF CUstomer Feedback
//
//  Created by Anik on 2017-09-23.
//  Copyright © 2017 CNFITCommunity Natural Foods. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController {

    var locations :Array = [AnyObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.backgroundView = UIImageView(image: UIImage(named: "white and gray.jpg"))
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        //set transparent navigation bar
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        
        //set navigation bar title color white
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor :UIColor.black]
        
        //read location file
        readLocations()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return locations.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "storeCells", for: indexPath)

        cell.textLabel?.text = locations[indexPath.row].value(forKey: "Description") as? String
        cell.textLabel?.textColor = UIColor.black
        cell.textLabel?.font = UIFont(name:"Helvetica-Light", size:30)
        return cell
    }
    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return false
    }
 
//    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//            }
    

    //MARK: - read data file function
    
    func readLocations(){
        
        if let filepath = Bundle.main.path(forResource: "location", ofType: "json")
        {
            do
            {
                let contents:String = try String(contentsOfFile: filepath)
                let data: Data = contents.data(using: String.Encoding.utf8, allowLossyConversion: true)!
                let message:Any = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                let locationArray = (message as! NSArray) as Array
                locations=locationArray
            }
            catch
            {
                print("Cannot read location file")
                locations=[]
            }
        }
        else
        {
            print("Cannot find location file")
            locations=[]
        }

    }

    //MARK: - segue funcitons
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let home = segue.destination as? HomeViewController {
            home.location = locations[(self.tableView.indexPathForSelectedRow?.row)!] as? Dictionary<String, String>
        }
    }
    
}
