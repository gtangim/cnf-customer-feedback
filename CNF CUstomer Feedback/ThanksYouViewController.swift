//
//  ThanksYouViewController.swift
//  CNF CUstomer Feedback
//
//  Created by Anik on 2017-09-24.
//  Copyright © 2017 CNFITCommunity Natural Foods. All rights reserved.
//

import UIKit
import CoreData


class ThanksYouViewController: UIViewController {

    @IBOutlet weak var gifViewer: UIImageView!
    var locationId : String = ""
    var feedback : String = ""
    var reason : String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        //gifViewer.loadGif(name: "giphy-6")
        
        //save things to DB

        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        let customerFeedback = NSEntityDescription.insertNewObject(forEntityName: "CustomerFeedback", into: context) 

        customerFeedback.setValue(Date(), forKey: "dateTime")
        customerFeedback.setValue(feedback, forKey: "feedback")
        customerFeedback.setValue(reason, forKey: "reason")
        customerFeedback.setValue(locationId, forKey: "storeLocation")
        customerFeedback.setValue(false, forKey: "uploaded")
        
        do {
            try context.save()
        } catch {
            fatalError("Failure to save context: \(error)")
        }
        
        
        // Do any additional setup after loading the view.
    }

    
    override func viewDidAppear(_ animated: Bool) {
        sleep(3)
        self.navigationController?.popToViewController((self.navigationController?.viewControllers[1])!, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
