//
//  FeedbackReasonTableViewController.swift
//  CNF CUstomer Feedback
//
//  Created by Anik on 2017-09-26.
//  Copyright © 2017 CNFITCommunity Natural Foods. All rights reserved.
//

import UIKit

class FeedbackReasonTableViewController: UITableViewController {

    var reasons :Array = [String]()
    var locationId : String = ""
    var feedback : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        reasons += ["Queue was too long", "Not Enough Till", "Poor Customer Service", "Inadequate Staff"]
        
        tableView.backgroundView = UIImageView(image: UIImage(named: "blue-ipad-wallpapers.jpg"))
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
//        //set transparent navigation bar
//        
//        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
//        self.navigationController?.navigationBar.shadowImage = UIImage()
//        self.navigationController?.navigationBar.isTranslucent = true
//        
//        //set navigation bar title color white
//        
//        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName :UIColor.white]

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    public override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 300
    }
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reasons.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reasonCells", for: indexPath)

        cell.textLabel?.text = reasons[indexPath.row]
        cell.textLabel?.textColor = UIColor.white
        cell.textLabel?.font = UIFont(name:"Helvetica-Light", size:30)

        return cell
    }
    
    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return false
    }
 

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let thanks = segue.destination as? ThanksYouViewController {
            thanks.locationId = locationId
            thanks.feedback = feedback
            thanks.reason = reasons[(tableView.indexPathForSelectedRow?.row)!]
        }
    }
    

}
